# Cucumber, typescript and SWC

## What to run

    $ time npm run acceptance

## Concept

The idea is to precompile acceptance tests written in gherkin, instead of using
ts-node or tsm that will mount the whole source code in memory. 

## Unit tests

    $ time npm run test

Unit tests, use also a pre-compilation phase. Let's get rid of jest.

## Benchmark

On my machine (Mac book pro M1), compiling and running all of this (`npm run acceptance`) takes less than a second.