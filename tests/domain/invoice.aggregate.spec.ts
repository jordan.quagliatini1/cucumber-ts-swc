import { suite } from "uvu";
import { expect } from "expect";
import { Invoice, InvoiceCreatedEvent } from "../../src/domain/invoice";

const it = suite("InvoiceAggregate");

it("should create", function () {
  const got = Invoice.create({ amount: 100_00, number: "INV-0001" });
  expect(got.events).toContain(expect.any(InvoiceCreatedEvent));
});

it.run();
