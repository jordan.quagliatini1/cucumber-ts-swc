import { z } from "zod";

export class Invoice {
  private readonly _events: InvoiceCreatedEvent[] = [];
  get events(): readonly InvoiceCreatedEvent[] {
    return this._events;
  }

  static create(props: CreateInvoiceProps) {
    const validProps = CreateInvoicePropsValidator.parse(props);
    const self = new this();
    self._events.push(
      new InvoiceCreatedEvent(validProps.number, validProps.amount)
    );
    return self;
  }
}

export class InvoiceCreatedEvent {
  constructor(readonly number: string, readonly amount: number) {}
}

const CreateInvoicePropsValidator = z.object({
  number: z.string().min(1),
  amount: z.number().min(0),
});

type CreateInvoiceProps = z.infer<typeof CreateInvoicePropsValidator>;
