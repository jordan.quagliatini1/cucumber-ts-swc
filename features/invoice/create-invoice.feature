Feature: Invoice

    Scenario: Create an invoice
        When I create an invoice using
            """
                {
                    "amount": 10000,
                    "number": "INV-0001"
                }
            """
        Then a "InvoiceCreatedEvent" should be emitted