import { Then } from "@cucumber/cucumber";
import expect from "expect";

Then("a {string} should be emitted", function (eventName) {
    expect(this.invoice.events.map(event => event.constructor.name))
        .toContain(eventName)
});
