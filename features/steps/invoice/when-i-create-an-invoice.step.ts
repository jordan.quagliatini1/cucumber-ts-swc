import { When } from "@cucumber/cucumber";
import { Invoice } from "../../../src/domain/invoice";

When("I create an invoice using", function (docString) {
  this.invoice = Invoice.create(JSON.parse(docString))
});
