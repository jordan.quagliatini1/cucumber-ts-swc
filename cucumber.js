const path = require('node:path')

module.exports = {
    default: {
        format: ['progress'],
        require: [
            path.join(__dirname, 'dist', 'features', 'steps', '**', '*.js')
        ]
    }
}